# General

All discussions about the organization of the teams within the Defend stage.

Discussions related to Product development must happen in [gitlab](https://gitlab.com/gitlab-org/gitlab) issue trackers.

## Outgoing MRs

As a part of [our Engineering Metrics](https://about.gitlab.com/handbook/engineering/metrics/),
we aim at measuring [Merge Request rates](https://about.gitlab.com/handbook/engineering/metrics/#merge-request-rate) both internally and externally. The list
below covers our outgoing MRs per milestone to community open source projects.

### %16.3

- sigstore/docs: [Add a link to GitLab's signing documentation](https://github.com/sigstore/docs/pull/200)

### %15.11

- sigstore/fulcio: [Map GitLab OIDC token claims to Fulcio OIDs](https://github.com/sigstore/fulcio/pull/1097)

### %15.9

- unleash/unleash-client-ruby: [Fix murmurhash3 dependency version to ensure native libraries are used](https://github.com/Unleash/unleash-client-ruby/pull/133)

### %15.3

- aquasecurity/trivy: [fix(vuln): GitLab report template](https://github.com/aquasecurity/trivy/pull/2578)
- rubyzip/rubyzip: [Fix unraised error on encrypted archives](https://github.com/rubyzip/rubyzip/pull/534)

### %15.2

- anchore/grype: [fix: accept templates with custom functions](https://github.com/anchore/grype/pull/786)
- davishmcclurg/json_schemer: [Add ability to change Regexp class used to validate schema](https://github.com/davishmcclurg/json_schemer/pull/111) (contribution not accepted)

### %15.1

- github/ssh_data: [Verify using user-provided public key](https://github.com/github/ssh_data/pull/35) (contribution not accepted)
- github/ssh_data: [Refactor signature spec](https://github.com/github/ssh_data/pull/36)

### %15.0

- aquasecurity/trivy: [Fix default value of --security-checks in docs](https://github.com/aquasecurity/trivy/pull/2107)
- aquasecurity/trivy: [Include GitLab 15.0 information in integation docs](https://github.com/aquasecurity/trivy/pull/2153)

### %14.10

- aquasecurity/trivy: [Add `--db-repository` flag to get advisory database from OCI registry](https://github.com/aquasecurity/trivy/pull/1873)
- aquasecurity/starboard: [Use dbRepository flag to get advisory database from OCI registry](https://github.com/aquasecurity/starboard/pull/1064)

### %14.9

- aquasecurity/trivy-db: [Improve error messaging when vulnerability details are missing](https://github.com/aquasecurity/trivy-db/pull/190)

### %14.7

- falcosecurity/charts: [Add configuration value to run unprivileged with capabilities](https://github.com/falcosecurity/charts/pull/305)

### %13.3

- cilium: [Use the global Azure key](https://github.com/cilium/cilium/pull/12683)

### %13.1

 - cilium: [Fix FilterLine test matchers and related specs](https://github.com/cilium/cilium/pull/11794)
 - cilium: [Add audit action to the policy verdict log](https://github.com/cilium/cilium/pull/11843)

### %12.10

 - cilium: [Handle audit mode in cilium endpoint list and kubectl get cep](https://github.com/cilium/cilium/pull/11011)

### %12.9

 - hubble: [Add options to the flow metrics](https://github.com/cilium/hubble/pull/117) 
 - cilium: [Improve pod restarts on GKE](https://github.com/cilium/cilium/pull/10377)

### %12.8

 - cilium: [Implement policy audit mode for the daemon](https://github.com/cilium/cilium/pull/9970)

### %12.7

 - ingress-nginx: [Update Modsecurity-nginx to latest (v1.0.1)](https://github.com/kubernetes/ingress-nginx/pull/4842)
 - cilium: [Improve nodeinit uninstalls by reverting nodeinit changes](https://github.com/cilium/cilium/pull/9757)
 - cilium: [Add cilium-monitor sidecar container for agent pods](https://github.com/cilium/cilium/pull/9815)
 - cilium: [Add helm charts packaging steps to the release script](https://github.com/cilium/cilium/pull/9772)
 - cilium: [Use helm repository in docs](https://github.com/cilium/cilium/pull/9783)
 - cilium: [Fix helm subchart versions](https://github.com/cilium/cilium/pull/9826)
